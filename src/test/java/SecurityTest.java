/**
 * Created by vadamson on 8/26/2015.
 */

import com.panera.security.globalauth.GlobalAuthProperties;
import com.panera.security.globalauth.model.User;
import com.panera.security.globalauth.GlobalAuthClient;
 import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
//import org.springframework.boot.test.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by vadamson on 8/11/2015.
 */

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration( initializers = ConfigFileApplicationContextInitializer.class)

public class SecurityTest {




    @Test
    public void getUser(){

        try {

            User user = GlobalAuthClient.getUser("Vadamson2", "Bread1234");
            Assert.assertTrue(user.getUsername().equals("vadamson"));

        }catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Test
    public void getUserFailed(){
        User user = GlobalAuthClient.getUser("Vadamson2","Failed");
        Assert.assertTrue(user.getUsername().equals("vadamson"));

    }




}
