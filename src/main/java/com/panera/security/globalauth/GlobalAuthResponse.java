package com.panera.security.globalauth;

/**
 * Created by vadamson on 7/21/2015.
 */
public class GlobalAuthResponse {

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    private String error;
    private String message;
    private Boolean status;

}