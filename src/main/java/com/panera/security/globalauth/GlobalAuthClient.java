package com.panera.security.globalauth;

import com.panera.security.globalauth.model.Group;
import com.panera.security.globalauth.model.GroupList;
import com.panera.security.globalauth.model.User;
import com.panera.security.globalauth.model.UsernamePassword;
//import com.panera.security.weeklyscorecard.ApplicationContextHolder;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import sun.security.provider.PolicyParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by vadamson on 7/29/2015.
 */
public class GlobalAuthClient {

    private static String globalAuthUrl = GlobalAuthProperties.getGlobalAuthUrl(); // ApplicationContextHolder.getContext().getEnvironment().getProperty("globalauth.url");
    private static String globalAuthToken = GlobalAuthProperties.getGlobalAuthToken(); // ApplicationContextHolder.getContext().getEnvironment().getProperty("globalauth.token");

    public static User getUser(String username, String password)throws RestClientException {

         RestTemplate restTemplate = new RestTemplate();
         restTemplate.getMessageConverters().add(new org.springframework.http.converter.json.MappingJackson2HttpMessageConverter());



         try {

            UsernamePassword usernamepassword = new UsernamePassword();

            usernamepassword.setUsername(username.trim());
            usernamepassword.setPassword(password.trim());


            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<UsernamePassword> entity = new HttpEntity<UsernamePassword>(usernamepassword, headers);

            UriComponentsBuilder uri = UriComponentsBuilder.fromUriString(globalAuthUrl)
                    .queryParam("applicationToken", globalAuthToken)
                    .path("user");

            //TODO: Pool Rest Client
            ResponseEntity<User> result = restTemplate.exchange(uri.build().toString(), HttpMethod.POST, entity, User.class);

            System.out.println(result);

            User user = result.getBody();

            List<Group> groups = getUserGroups(username, password).getGroupList();
            user.setRoles(new ArrayList<GrantedAuthority>(groups));

            return user;

        } catch (RestClientException ex) {


           throw ex;

        }

    }

    public static GroupList getUserGroups(String username, String password) throws RestClientException {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());


        try {

            UsernamePassword usernamepassword = new UsernamePassword();

            usernamepassword.setUsername(username.trim());
            usernamepassword.setPassword(password.trim());

             HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<Object> entity = new HttpEntity<Object>(usernamepassword, headers);

            UriComponentsBuilder uri = UriComponentsBuilder.fromUriString(globalAuthUrl)
                    .queryParam("applicationToken", globalAuthToken)
                    .path("user/groups");

            ResponseEntity<GroupList> result = restTemplate.exchange(uri.build().toString(), HttpMethod.POST, entity, GroupList.class);

            System.out.println(result);

            GroupList groupList = result.getBody();

            return groupList;

        } catch (RestClientException ex) {

            throw ex;

        }

    }



}
