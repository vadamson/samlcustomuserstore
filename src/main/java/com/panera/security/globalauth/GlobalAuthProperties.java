package com.panera.security.globalauth;

import java.util.Map;

/**
 * Created by vadamson on 8/26/2015.
 */
public class GlobalAuthProperties {

      private static  String GlobalAuthUrl = "https://globalauthtest.panerabread.com/GlobalAuthentication/";
      private static  String GlobalAuthToken = "PaneraBOH1234";
      private static  String GlobalAuthTimeOut = "2000";

    protected static String getGlobalAuthTimeOut() {
        return GlobalAuthTimeOut;
    }

    public static void setGlobalAuthTimeOut(String globalAuthTimeOut) {
        GlobalAuthTimeOut = globalAuthTimeOut;
    }

    protected static String getGlobalAuthToken() {
        return GlobalAuthToken;
    }

    public static void setGlobalAuthToken(String globalAuthToken) {
        GlobalAuthToken = globalAuthToken;
    }

    protected static String getGlobalAuthUrl() {
        return GlobalAuthUrl;
    }

    public static void setGlobalAuthUrl(String globalAuthUrl) {
        GlobalAuthUrl = globalAuthUrl;
    }

    public static void init(Map<String, String> properties) {

        GlobalAuthProperties.setGlobalAuthTimeOut(properties.get("GlobalAuthTimeOut"));
        GlobalAuthProperties.setGlobalAuthToken(properties.get("GlobalAuthToken"));
        GlobalAuthProperties.setGlobalAuthUrl(properties.get("GlobalAuthUrl"));

    }
}
