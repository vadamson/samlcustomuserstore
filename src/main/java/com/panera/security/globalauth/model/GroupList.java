package com.panera.security.globalauth.model;

import org.springframework.security.core.GrantedAuthority;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by vadamson on 8/4/2015.
 */
public class GroupList  {

    private List<Group> groupList;

    public List<Group> getGroupList() {
        if(groupList == null){
            groupList = new LinkedList<Group>();
        }
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }
}
