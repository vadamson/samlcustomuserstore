package com.panera.security.globalauth.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;

import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by vadamson on 7/29/2015.
 */
public class User {

    private String username;
    private String hrEmployeeNbr;
    private String payrollEmployeeNbr;
    private String fullName;
    private String firstName;
    private String lastName;
    private String knownAs;
    private String emailAddress;
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private GregorianCalendar dateOfBirth;
    private String orgName;
    private String company;
    private String payrollId;
    private String orgLocationCd;
    private String orgAddressLine1;
    private String orgAddressLine2;
    private String orgAddressLine3;
    private String orgTownOrCity;
    private String orgRegion1;
    private String orgRegion2;
    private String orgPostalCd;
    private String orgCountry;
    private String positionSegment1;
    private String positionSegment2;
    private String positionSegment3;
    private List<GrantedAuthority> roles;



    @JsonIgnore
    private GregorianCalendar startDateActive;
    @JsonIgnore
    private GregorianCalendar endDateActive;
    @JsonIgnore
    private String lastFourOfNationalId;

    public User() { }

    public User(String username) {
        setUsername(username);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHrEmployeeNbr() {
        return hrEmployeeNbr;
    }

    public void setHrEmployeeNbr(String hrEmployeeNbr) {
        this.hrEmployeeNbr = hrEmployeeNbr;
    }

    public String getPayrollEmployeeNbr() {
        return payrollEmployeeNbr;
    }

    public void setPayrollEmployeeNbr(String payrollEmployeeNbr) {
        this.payrollEmployeeNbr = payrollEmployeeNbr;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getKnownAs() {
        return knownAs;
    }

    public void setKnownAs(String knownAs) {
        this.knownAs = knownAs;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public GregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(GregorianCalendar dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPayrollId() {
        return payrollId;
    }

    public void setPayrollId(String payrollId) {
        this.payrollId = payrollId;
    }

    public String getOrgLocationCd() {
        return orgLocationCd;
    }

    public void setOrgLocationCd(String orgLocationCd) {
        this.orgLocationCd = orgLocationCd;
    }

    public String getOrgAddressLine1() {
        return orgAddressLine1;
    }

    public void setOrgAddressLine1(String orgAddressLine1) {
        this.orgAddressLine1 = orgAddressLine1;
    }

    public String getOrgAddressLine2() {
        return orgAddressLine2;
    }

    public void setOrgAddressLine2(String orgAddressLine2) {
        this.orgAddressLine2 = orgAddressLine2;
    }

    public String getOrgAddressLine3() {
        return orgAddressLine3;
    }

    public void setOrgAddressLine3(String orgAddressLine3) {
        this.orgAddressLine3 = orgAddressLine3;
    }

    public String getOrgTownOrCity() {
        return orgTownOrCity;
    }

    public void setOrgTownOrCity(String orgTownOrCity) {
        this.orgTownOrCity = orgTownOrCity;
    }

    public String getOrgRegion1() {
        return orgRegion1;
    }

    public void setOrgRegion1(String orgRegion1) {
        this.orgRegion1 = orgRegion1;
    }

    public String getOrgRegion2() {
        return orgRegion2;
    }

    public void setOrgRegion2(String orgRegion2) {
        this.orgRegion2 = orgRegion2;
    }

    public String getOrgPostalCd() {
        return orgPostalCd;
    }

    public void setOrgPostalCd(String orgPostalCd) {
        this.orgPostalCd = orgPostalCd;
    }

    public String getOrgCountry() {
        return orgCountry;
    }

    public void setOrgCountry(String orgCountry) {
        this.orgCountry = orgCountry;
    }

    public String getPositionSegment1() {
        return positionSegment1;
    }

    public void setPositionSegment1(String positionSegment1) {
        this.positionSegment1 = positionSegment1;
    }

    public String getPositionSegment2() {
        return positionSegment2;
    }

    public void setPositionSegment2(String positionSegment2) {
        this.positionSegment2 = positionSegment2;
    }

    public String getPositionSegment3() {
        return positionSegment3;
    }

    public void setPositionSegment3(String positionSegment3) {
        this.positionSegment3 = positionSegment3;
    }

    public GregorianCalendar getStartDateActive() {
        return startDateActive;
    }

    public void setStartDateActive(GregorianCalendar startDateActive) {
        this.startDateActive = startDateActive;
    }

    public GregorianCalendar getEndDateActive() {
        return endDateActive;
    }

    public void setEndDateActive(GregorianCalendar endDateActive) {
        this.endDateActive = endDateActive;
    }

    public String getLastFourOfNationalId() {
        return lastFourOfNationalId;
    }

    public void setLastFourOfNationalId(String lastFourOfNationalId) {
        this.lastFourOfNationalId = lastFourOfNationalId;
    }

    public List<GrantedAuthority> getRoles() {
        return roles;
    }

    public void setRoles(List<GrantedAuthority> roles) {
        this.roles = roles;
    }
}

