package com.panera.security.globalauth.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by vadamson on 7/29/2015.
 */


public class UsernamePassword {

    private String username;
    private String password;
    private String newPassword;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}