package com.panera.security.globalauth.model;

import org.springframework.security.core.GrantedAuthority;

/**
 * Created by vadamson on 8/4/2015.
 */
public class Group implements GrantedAuthority {

    private String name;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

     public String getAuthority() {
        return this.name;
    }
}
