package com.panera.wso2.custom.usermgt;

import com.panera.security.globalauth.GlobalAuthClient;
import com.panera.security.globalauth.GlobalAuthProperties;
import com.panera.security.globalauth.model.GroupList;
import com.panera.security.globalauth.model.User;
import org.wso2.carbon.user.api.RealmConfiguration;
import org.wso2.carbon.user.core.UserRealm;
import org.wso2.carbon.user.core.UserStoreException;
import org.wso2.carbon.user.core.claim.ClaimManager;
import org.wso2.carbon.user.core.jdbc.JDBCUserStoreManager;
import org.wso2.carbon.user.core.profile.ProfileConfigurationManager;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/**
 * Created by vadamson on 8/18/2015.
 */
public class CustomUserStoreManager extends JDBCUserStoreManager{


    private User user;
    private GroupList userGroupMemembership;


    public GroupList getUserGroupMemembership() {
        return userGroupMemembership;
    }


    private static Log log = LogFactory.getLog(CustomUserStoreManager.class);
    // This instance is used to generate the hash values

    // You must implement at least one constructor
    public CustomUserStoreManager(RealmConfiguration realmConfig, Map<String, Object> properties, ClaimManager
            claimManager, ProfileConfigurationManager profileManager, UserRealm realm, Integer tenantId)
            throws UserStoreException {

        super(realmConfig, properties,claimManager, profileManager,realm, tenantId);

            log.info("GlobalAuth Initialization Started");
             GlobalAuthProperties.init(realm.getRealmConfiguration().getUserStoreProperties());
            log.info("GlobalAuth Initializing Complete");
     }

    @Override
    public boolean doAuthenticate(String userName, Object credential) throws org.wso2.carbon.user.core.UserStoreException {
       // return super.doAuthenticate(userName, credential);

        try {

            if (userName.equals("admin")) return super.doAuthenticate(userName, credential);

            user = GlobalAuthClient.getUser(userName, credential.toString());

            userGroupMemembership = GlobalAuthClient.getUserGroups(userName,credential.toString());

            return true;

        }catch(Exception ex) {
            log.info("doAuthenticate Error: " + ex.getMessage());
            log.info("Stack Trace: " + ex.getStackTrace());
            UserStoreException usex = new UserStoreException("Unauthorized!");
            usex.setStackTrace(ex.getStackTrace());

            throw usex;

        }
    }

    @Override
    protected String preparePassword(String password, String saltValue) throws org.wso2.carbon.user.core.UserStoreException {

        return super.preparePassword(password, saltValue);

    }


    @Override
    public Map<String, String> getUserPropertyValues(String userName, String[] propertyNames, String profileName) throws UserStoreException {

        Map<String, String> properties = new HashMap<String, String>();


        properties.put("givenName",this.user.getFirstName()); //firstName
        properties.put("sn",this.user.getLastName()); //lastName
        properties.put("cn", this.user.getFullName()); //fullName
        properties.put("hrEmployeeNbr", this.user.getHrEmployeeNbr()); //hrEmployeeNbr
        properties.put("payrollEmployeeNbr", this.user.getPayrollEmployeeNbr()); //payrollEmployeeNbr
        properties.put("payrollId",this.user.getPayrollId()); //payrollId
        properties.put("nickName",this.user.getKnownAs()); //knownAs
        properties.put("dateOfBirth",this.user.getDateOfBirth().toString()); //dateOfBirth
        properties.put("company",this.user.getCompany()); //company
        properties.put("organizationName", this.user.getOrgName()); //orgName
        properties.put("orgLocationCd", this.user.getOrgLocationCd()); //orgLocationCd
        properties.put("orgAddressLine1", this.user.getOrgAddressLine1()); //orgAddressLine1
        properties.put("orgAddressLine2", this.user.getOrgAddressLine2()); //orgAddressLine2
        properties.put("orgTownOrCity",this.user.getOrgTownOrCity()); //orgTown
        properties.put("orgRegion1", this.user.getOrgRegion1()); //orgRegion1
        properties.put("orgRegion2", this.user.getOrgRegion2()); //orgRegion2
        properties.put("orgPostalCd",this.user.getOrgPostalCd());//orgPostalCd
        properties.put("orgCountry",this.user.getOrgCountry());//orgCountry
        properties.put("positionSegment1", this.user.getPositionSegment1());//positionSegment1
        properties.put("positionSegment2", this.user.getPositionSegment2());//positionSegment2
        properties.put("positionSegment3", this.user.getPositionSegment3());//positionSegment3
        properties.put("mail",this.user.getEmailAddress());


        return properties;

    }
}
